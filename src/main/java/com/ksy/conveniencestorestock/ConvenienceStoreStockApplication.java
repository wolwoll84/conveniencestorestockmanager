package com.ksy.conveniencestorestock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConvenienceStoreStockApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConvenienceStoreStockApplication.class, args);
    }

}
