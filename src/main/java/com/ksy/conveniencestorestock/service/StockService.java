package com.ksy.conveniencestorestock.service;

import com.ksy.conveniencestorestock.entity.Stock;
import com.ksy.conveniencestorestock.model.SetStockRequest;
import com.ksy.conveniencestorestock.model.StockItem;
import com.ksy.conveniencestorestock.model.StockQuantityUpdateRequest;
import com.ksy.conveniencestorestock.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class StockService {
    private final StockRepository stockRepository;

    public void setData(SetStockRequest request) {
        Stock addData = new Stock();
        addData.setBarcode(request.getBarcode());
        addData.setCategory(request.getCategory());
        addData.setProductName(request.getProductName());
        addData.setAddQuantity(request.getAddQuantity());
        addData.setStockQuantity(request.getAddQuantity());
        addData.setExpirationDate(request.getExpirationDate());
        addData.setPrice(request.getPrice());
        addData.setIsTax(request.getIsTax());
        addData.setReceivingDate(LocalDate.now());

        stockRepository.save(addData);
    }

    public List<StockItem> getStocks() {
        List<StockItem> result = new LinkedList<>();

        List<Stock> allData = stockRepository.findAll();

        for (Stock item : allData) {
            StockItem addItem = new StockItem();

            addItem.setId(item.getId());
            addItem.setBarcode(item.getBarcode());
            addItem.setCategory(item.getCategory());
            addItem.setProductName(item.getProductName());
            addItem.setAddQuantity(item.getAddQuantity());
            addItem.setSoldQuantity(item.getSoldQuantity());
            addItem.setStockQuantity(item.getStockQuantity());
            addItem.setExpirationDate(item.getExpirationDate());

            String isExpirationDate = "정상";
            if (item.getExpirationDate().isBefore(LocalDate.now())) {
                isExpirationDate = "폐기";
            } else if (item.getExpirationDate().minusDays(1).equals(LocalDate.now())) {
                isExpirationDate = "임박";
            }

            addItem.setIsExpirationDate(isExpirationDate);

            addItem.setPrice(item.getPrice());
            addItem.setIsTax(item.getIsTax());
            addItem.setReceivingDate(item.getReceivingDate());

            result.add(addItem);
        }

        return result;
    }

    public void putSold(long id, StockQuantityUpdateRequest request) {
        Stock originData = stockRepository.findById(id).orElseThrow();
        originData.setSoldQuantity(request.getSoldQuantity());
        originData.setStockQuantity(originData.getAddQuantity() - request.getSoldQuantity());

        stockRepository.save(originData);
    }

    public void delData(long id) {
        stockRepository.deleteById(id);
    }
}
