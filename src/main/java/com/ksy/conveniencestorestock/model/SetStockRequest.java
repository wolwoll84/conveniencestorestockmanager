package com.ksy.conveniencestorestock.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class SetStockRequest {
    @NotNull
    @Length(min = 8, max = 8)
    private String barcode;

    @NotNull
    @Length(min = 1, max = 10)
    private String category;

    @NotNull
    @Length(min = 1, max = 50)
    private String productName;

    @NotNull
    @Min(value = 0)
    @Max(value = 500)
    private Integer addQuantity;

    private LocalDate expirationDate;

    @NotNull
    @Min(value = 0)
    private Integer price;

    @NotNull
    private Boolean isTax;
}
