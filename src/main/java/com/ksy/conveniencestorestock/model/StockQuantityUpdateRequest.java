package com.ksy.conveniencestorestock.model;

import com.ksy.conveniencestorestock.entity.Stock;
import com.ksy.conveniencestorestock.repository.StockRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class StockQuantityUpdateRequest {
    @NotNull
    @Min(value = 1) // 나중에 재고 수량을 초과하면 입력할 수 없게끔 수정하기
    private Integer soldQuantity;
}
