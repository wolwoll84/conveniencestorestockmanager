package com.ksy.conveniencestorestock.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StockItem {
    private Long id;
    private String barcode;
    private String category;
    private String productName;
    private Integer addQuantity;
    private Integer soldQuantity;
    private Integer stockQuantity;
    private LocalDate expirationDate;
    private String isExpirationDate;
    private Integer price;
    private Boolean isTax;
    private LocalDate receivingDate;
}
