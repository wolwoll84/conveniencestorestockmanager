package com.ksy.conveniencestorestock.repository;

import com.ksy.conveniencestorestock.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Long> {
}
