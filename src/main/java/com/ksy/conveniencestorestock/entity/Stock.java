package com.ksy.conveniencestorestock.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 8)
    private String barcode;

    @Column(nullable = false, length = 10)
    private String category;

    @Column(nullable = false, length = 50)
    private String productName;

    @Column(nullable = false)
    private Integer addQuantity;

    private Integer soldQuantity;

    private Integer stockQuantity;

    private LocalDate expirationDate;

    @Column(nullable = false)
    private Integer price;

    @Column(nullable = false)
    private Boolean isTax;

    @Column(nullable = false)
    private LocalDate receivingDate;

}
