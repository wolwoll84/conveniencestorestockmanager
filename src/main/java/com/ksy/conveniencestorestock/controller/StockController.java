package com.ksy.conveniencestorestock.controller;

import com.ksy.conveniencestorestock.model.SetStockRequest;
import com.ksy.conveniencestorestock.model.StockItem;
import com.ksy.conveniencestorestock.model.StockQuantityUpdateRequest;
import com.ksy.conveniencestorestock.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/stock")
public class StockController {
    private final StockService stockService;

    @PostMapping("/data")
    public String setData(@RequestBody @Valid SetStockRequest request) {
        stockService.setData(request);

        return "등록했습니다.";
    }

    @GetMapping("/all")
    public List<StockItem> getStocks() {
        return stockService.getStocks();
    }

    @PutMapping("/sold/id/{id}")
    public String putSold(@PathVariable long id, @RequestBody @Valid StockQuantityUpdateRequest request) {
        stockService.putSold(id, request);

        return "판매 수량 입력했습니다.";
    }

    @DeleteMapping("/end-sale/id/{id}")
    public String delData(@PathVariable long id) {
        stockService.delData(id);

        return "삭제했습니다.";
    }
}
